include: "snakemake"

import glob
#https://github.com/leipzig/SandwichesWithSnakemake/blob/master/glob.snake
rule heatmap_fpkm:
    input: SAMPLES, COUNTS
    output: heat="heatmap.pdf", fpkm="fpkm", pca="pca_vst_base.pdf"
    params: threads=CORES
    script: SRC_DIR+"/heat_fpkm.R"

rule preliminary_deseq:
    input: SAMPLES, SAMPLES_REMOVE, COUNTS
    output: "prelim_DESeq2.Rdata"
    params: threads=CORES,mincounts=MINCOUNTS, mingroup=MINGROUP
    script: SRC_DIR+"/deseq_prelim.R"

rule all_lps:
    input: expand("cutoff0.05-{comp}.deseq2.tsv", comp=COMPARISONS_LPS)

rule all_ut:
    input: expand("cutoff0.05-{comp}.deseq2.tsv", comp=COMPARISONS_UT)

rule deseq:
    input: "prelim_DESeq2.Rdata"
    output: tsv="cutoff{alpha}-{nom}.vs.{den}.deseq2.tsv", volcano="cutoff{alpha}-{nom}.vs.{den}.deseq2.pdf"
    params: threads=CORES, alpha="{alpha}", factor="comparison", nom="{nom}", den="{den}", lfc=LFC, mincounts=MINCOUNTS, mingroup=MINGROUP
    script: SRC_DIR+"/deseq_diff.R"
